<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ReservationRequest;
use Illuminate\Support\Facades\Crypt;

class ReservationController extends Controller
{
    public function show()
    {
    	$cities=self::get_api_data('buses');
    	$cities=json_encode(trim($cities), TRUE);
    	var_dump($cities);
    	echo "<br>in...<br>";
    	$jsonData = $cities;
    	$jsonData = rtrim($jsonData, "\0");
    	$jsonData = json_decode(trim($jsonData), TRUE);

print_r($jsonData);
    	
    	die();
    	if (Auth::user()){	
        	return view('reservation.index');
        }else{
        	return redirect('/login');
        }
    }

    public function book(ReservationRequest $request) 
    {
        $reservation = Reservation::create($request->validated());

        $insertedId = $reservation->id;
        $insId= Crypt::encrypt($insertedId);

        if($insertedId){
        	return redirect('/reservation/'.$insId);
        }else{
        	return redirect('/reservation')->with('error', "Something went wrong, please try again.");
        }

        
    }

     public function view($id){
     	$resid = Crypt::decrypt($id);
     	$data = Reservation::find($resid);
     	$apidata = array(
		    'start_location' => 'Colombo',
		    'end_location' => 'Kandy',
		    'bus_no' => 'KR 1085',
		    'bus_name' => 'ABC Travels',
		    'start-time' => '08.30 PM',
		);
		     	return view('reservation.view')->with('data',$data)->with('apidata', $apidata);
     }

     public function get_api_data($requesturl){
     	$burl="http://localhost/VV/ticket-reservation/public/";
	    $url = $burl."api/".$requesturl;
	    $ch = curl_init($url);
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
	    $err = curl_error($ch);
	    $response = curl_exec($ch);
	    if ($err) {
	     echo "cURL Error #:" . $err;
	    }
	    curl_close($ch);
	    return $response;
	}
}
